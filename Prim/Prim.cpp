#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <time.h>
#include <iostream>
#include <list>
using namespace std;


struct mPair
{
	int Vertex;
	int Weght;
	mPair(int v, int w):Vertex(v), Weght(w){}
};

struct mTriplet
{
	int Vertex1;
	int Weght;
	int Vertex2;
	mTriplet(int v1, int w, int v2):Vertex1(v1), Weght(w), Vertex2(v2){}
};

int** adjacency_matrix;
list<mPair>* adjacency_list;
const int p = 4;
int Number;
const int max_value = 100;
int last_added_vertex = 0;
mTriplet min_weigth(0,max_value,0);
int thread_counter = p;
int working_thread_counter = p;
HANDLE* barrierSemaphore;
HANDLE counterMutex;


DWORD WINAPI ThreadFunction(LPVOID lpParam)
{
	int name = (int)lpParam;

	list<mTriplet> working_list;
	for(int i=0; name + i*p < Number; i++)
		working_list.push_back(mTriplet(name + i*p,max_value,last_added_vertex));

	mTriplet Min(last_added_vertex,max_value,last_added_vertex);
	int min = max_value;
	int tree_vertex1 = last_added_vertex;

	while(!working_list.empty())
	{
		WaitForSingleObject(barrierSemaphore[name],INFINITE);
		list<mTriplet>::iterator i =  working_list.begin();
		while(i != working_list.end() && i->Vertex1 != last_added_vertex) i++;
		if(i != working_list.end()) { working_list.erase(i); Min.Weght = max_value;}
		if(!working_list.empty())
		{
			for(i =  working_list.begin(); i != working_list.end(); i++)
			{
				if(i->Weght > adjacency_matrix[i->Vertex1][last_added_vertex])
				{
					i->Vertex2 = last_added_vertex;
					i->Weght = adjacency_matrix[i->Vertex1][last_added_vertex];
				}
				if(i->Weght < Min.Weght)
				{
					Min.Weght = i->Weght;
					Min.Vertex1 = i->Vertex1;
					Min.Vertex2 = i->Vertex2;
				} 
			}
			WaitForSingleObject(counterMutex,INFINITE);
			if(Min.Weght < min_weigth.Weght)
			{
				min_weigth = Min;
			}
			if(thread_counter > 1) thread_counter--;
			else
			{
				last_added_vertex = min_weigth.Vertex1;
				adjacency_list[min_weigth.Vertex1].push_back(mPair(min_weigth.Vertex2,min_weigth.Weght));
				adjacency_list[min_weigth.Vertex2].push_back(mPair(min_weigth.Vertex1,min_weigth.Weght));
				min_weigth.Weght = max_value;
				for(int i = 0; i < p; i++)
					ReleaseSemaphore(barrierSemaphore[i],1,0);
				thread_counter = working_thread_counter;
			}
			ReleaseMutex(counterMutex);
		}
	}
	if(working_thread_counter > 1)
	{
		WaitForSingleObject(counterMutex,INFINITE);
			if(thread_counter > 1) 
			{thread_counter--; working_thread_counter--;}
			else
			{
				last_added_vertex = min_weigth.Vertex1;
				adjacency_list[min_weigth.Vertex1].push_back(mPair(min_weigth.Vertex2,min_weigth.Weght));
				adjacency_list[min_weigth.Vertex2].push_back(mPair(min_weigth.Vertex1,min_weigth.Weght));
				min_weigth.Weght = max_value;
				for(int i = 0; i < p; i++)
					ReleaseSemaphore(barrierSemaphore[i],1,0);
				thread_counter = --working_thread_counter;
			}
		ReleaseMutex(counterMutex);
	}
	
	return 0;
}


int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(0, "Russian");

	cout << "������� ����� ������\n";
	cin >> Number;

	cout<<"������� ������� ���������:\n";

	adjacency_matrix = new int*[Number];
	for(int i = 0; i < Number; i++)
		adjacency_matrix[i] = new int[Number];

	adjacency_list = new list<mPair>[Number];

	for(int i = 0; i < Number; i++)
		for(int j = 0; j < Number; j++)
			cin >> adjacency_matrix[i][j];
	
	barrierSemaphore = new HANDLE[p];
	for(int i=0; i<p; i++)
		barrierSemaphore[i] = CreateSemaphore(0,1,1,0);
	counterMutex = CreateMutex(0,0,0);


	HANDLE* threadContainer = new HANDLE[p];
	for(int i = 0; i < p; i++)
		threadContainer[i] = CreateThread(0,0,ThreadFunction,(LPVOID)i,0,0);

	WaitForMultipleObjects(p,threadContainer,true, INFINITE);

	for(int i = 0; i < Number; i++)
	{
		cout<<i<<": ";
		for(list<mPair>::iterator j =  adjacency_list[i].begin(); j !=  adjacency_list[i].end(); j++)
		{
			cout<<'('<<j->Vertex<<", "<<j->Weght<<"), ";
		}
		cout<<endl;
	}
	
	CloseHandle(counterMutex);
	for(int i=0; i<p; i++)
		CloseHandle(barrierSemaphore[i]);
	for(int i=0; i<p; i++)
		CloseHandle(threadContainer[i]);

	delete[]threadContainer;
	delete[]barrierSemaphore;
	for(int i = 0; i < Number; i++)
		delete[] adjacency_matrix[i];
	delete[]adjacency_matrix;
	delete[] adjacency_list;

	return 0;
}

